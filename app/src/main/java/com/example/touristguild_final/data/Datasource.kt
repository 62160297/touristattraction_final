package com.example.touristguild_final.data

import com.example.touristguild_final.R
import com.example.touristguild_final.model.TouristGuide

class Datasource() {

    fun loadAffirmations(): List<TouristGuide> {
        return listOf<TouristGuide>(
            TouristGuide("Railay Beach", R.drawable.place_1,"Krabi province is home to some of Thailand's"),
            TouristGuide("Koh Phi Phi", R.drawable.place_2, "The Phi Phi Islands are one of Thailand's most popular resort areas for a reason-the clear blue waters"),
            TouristGuide("The Grand Palace, Bangkok", R.drawable.place_3,"Even if your plans for Thailand mainly involve frolicking on a beach and eating as much Massaman curry and pad Thai as humanly possible"),
            TouristGuide("Sunday Walking Street, Chiang Mai", R.drawable.place_4,"Every Thailand visitor looks forward to cheap and delicious food-and that's exactly what they can find in abundance at Chiang Mai's Sunday Night Walking Street"),
            TouristGuide("Pai", R.drawable.place_5, "Thailand's reputation as a country of beautiful landscapes and friendly people is due largely to the world-renowned southern beaches"),
            TouristGuide("Wild Elephants at Khao Yai National Park", R.drawable.place_6,"Elephants are revered in Thailand, and statues and paintings of them can be seen everywhere you go"),
            TouristGuide("Sukhothai Old City", R.drawable.place_7, "A favorite stop for history buffs and photography enthusiasts, Sukhothai offers many lovely photo ops at a smaller scale than Ayutthaya"),
            TouristGuide("Historic City of Ayutthaya", R.drawable.place_8, "Ayutthaya offers a magnificent peek into the glory of ancient Thailand"),
            TouristGuide("Beaches of Koh Samui", R.drawable.place_9, "Koh Samui island is the country's second-largest island and home to some of the most beautiful golden coastline you'll find in Southeast Asia"),
            TouristGuide("Doi Suthep", R.drawable.place_10, "Perhaps the best-known wat in Chiang Mai sits atop Doi Suthep, a mountain overlooking Thailand's second-largest city")
        )
    }
}
package com.example.touristguild_final.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class TouristGuide(
    val placeTitle: String,
    val imageView: Int,
    val placeDetail: String)

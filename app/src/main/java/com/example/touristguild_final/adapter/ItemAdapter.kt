package com.example.touristguild_final.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.touristguild_final.R
import com.example.touristguild_final.databinding.CustomItemViewBinding
import com.example.touristguild_final.model.TouristGuide

class ItemAdapter(private val onItemClicked: (TouristGuide) -> Unit) :
    ListAdapter<TouristGuide, ItemAdapter.ItemViewHolder>(DiffCallback) {

    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<TouristGuide>() {
            override fun areItemsTheSame(oldItem: TouristGuide, newItem: TouristGuide): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: TouristGuide, newItem: TouristGuide): Boolean {
                return oldItem == newItem
            }
        }
    }

    class ItemViewHolder(private var binding: CustomItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(touristGuide: TouristGuide) {
            binding.cardImage.setImageResource(touristGuide.imageView)
            binding.placeTitle.text = touristGuide.placeTitle.toString()
            binding.placeDetail.text = touristGuide.placeDetail.toString()
        }
    }

    /**
     * Create new views (invoked by the layout manager)
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        // create a new view
        val viewHolder = ItemViewHolder(
            CustomItemViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.adapterPosition
            onItemClicked(getItem(position))
        }
        return viewHolder
    }

    /**
     * Replace the contents of a view (invoked by the layout manager)
     */
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    /**
     * Return the size of your dataset (invoked by the layout manager)
     */

}